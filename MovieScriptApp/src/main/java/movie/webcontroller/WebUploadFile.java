package movie.webcontroller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
class WebUploadFile {

	@RequestMapping("/")
	public ModelAndView index() {

		List<String> listaContatos = new ArrayList<String>();

		listaContatos.add("Pablo");
		listaContatos.add("Almerita");
		listaContatos.add("Maria Joaquina");
		listaContatos.add("Fernanda");
		listaContatos.add("Josy");

		ModelAndView model = new ModelAndView("home");
		model.addObject("mensagem", "Ola spring boot");
		model.addObject("autor", "Pablo Fodao cervejeiro");
		model.addObject("contatos", listaContatos);
		return model;
	}
}
