package demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MovieScriptApp {

    public static void main(String[] args) {
        SpringApplication.run(MovieScriptApp.class, args);
    }
}


