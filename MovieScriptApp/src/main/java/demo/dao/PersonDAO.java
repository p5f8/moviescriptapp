package demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import demo.model.Person;

@Repository
public interface PersonDAO extends JpaRepository<Person, Long>{

	Person findByName(String namePerson);
	
}
