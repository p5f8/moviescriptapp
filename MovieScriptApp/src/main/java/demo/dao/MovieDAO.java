package demo.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import demo.model.Movie;

@Repository
public interface MovieDAO extends CrudRepository<Movie, Long>{

	Movie findByName(String name);
	
}
