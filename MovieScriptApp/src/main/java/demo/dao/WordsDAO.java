package demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import demo.model.Words;

@Repository
public interface WordsDAO extends JpaRepository<Words, Long> {
	
}