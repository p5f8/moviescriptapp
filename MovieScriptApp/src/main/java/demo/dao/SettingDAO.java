package demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import demo.model.Setting;

@Repository
public interface SettingDAO extends JpaRepository<Setting, Long>{

	Setting findByName(String namePerson);
	
}
