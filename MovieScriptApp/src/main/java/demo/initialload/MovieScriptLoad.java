package demo.initialload;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.thoughtworks.xstream.converters.basic.StringBufferConverter;

import demo.dao.MovieDAO;
import demo.dao.PersonDAO;
import demo.dao.SettingDAO;
import demo.dao.WordsDAO;
import demo.model.Movie;
import demo.model.Person;
import demo.model.Setting;
import demo.model.Words;

@Component
public class MovieScriptLoad {

	private final Logger log = LoggerFactory.getLogger(MovieScriptLoad.class);
	
	private static final int INDENT_PERSON = 22;
	private static final int INDENT_DIALOGUE = 10;
	private static final int INDENT_MOVIE_SETTINGS = 0;
	
	private ClassLoader classLoader = getClass().getClassLoader();
	
	@Autowired
	private PersonDAO personDAO;
	
	@Autowired
	private SettingDAO settingDAO;
	
	@Autowired
	private WordsDAO wordsDAO;
	
	@Autowired
	private MovieDAO movieDAO;
	
	public MovieScriptLoad() { }
	
	public void process(BufferedReader input) throws IOException {
		
		boolean lookingForMovieName = true;
		
		int countSpaces = 0;
		
		Setting setting = null;
		Person person = null;
		
		log.info("====== starting reading movie script =========");
		
		String line = null;
		StringBuffer auxMovieName = new StringBuffer();
		
		while( (line = input.readLine()) != null) {
			
			
			try 
			{
			    byte[] lineBytes = line.getBytes("UTF-8");
			} 
			catch (UnsupportedEncodingException e)
			{
				// rethrow exception to user
				// line don't use charset utf-8
				throw e;
			}
			
			
			if(lookingForMovieName==true) {
				
				auxMovieName.append(line.trim());
				auxMovieName.append(" ");
				
				int position = auxMovieName.toString().toLowerCase().indexOf(" by ");
				if(position > -1 ) {
					String[] splitNameMovie = auxMovieName.toString().split(" ");
					auxMovieName = new StringBuffer();
					for( String word : splitNameMovie) {
						auxMovieName.append(word);
						auxMovieName.append(" ");
					}
					position = auxMovieName.toString().toLowerCase().indexOf(" by ");
					String nameMovie = auxMovieName.substring(0, position+1);
					
					Movie movie = movieDAO.findByName(nameMovie);
					if(movie==null) {
						log.info("Name movie screenplay received: " + nameMovie);
						
						movie = new Movie();
						movie.setName(nameMovie);
						movieDAO.save(movie);
						
						lookingForMovieName = false;	
					} else {
						log.error("Name movie screenplay received already exists and we didn't load this name: " + nameMovie);
						throw new RuntimeException("Name movie screenplay received already exists: " + nameMovie);
					}
				}
			}
			
			countSpaces = startWithHowMuchSpaces(line);
			
			if(countSpaces == INDENT_MOVIE_SETTINGS && isMovieSettings(line)) {
				String nameSetting = tryConvertNameSetting(line);
				
				setting = settingDAO.findByName(nameSetting);
				if(setting == null) {
					setting = new Setting();
					setting.setName(nameSetting);
					settingDAO.save(setting);
				}
			}
			
			if(countSpaces == INDENT_PERSON && line.equals(line.toUpperCase())) {
				
				String namePerson = line.trim();
				
				Person findPerson = personDAO.findByName(namePerson); 
				if(findPerson == null) {
					person = new Person();
					person.setName(namePerson);
					personDAO.save(person);
				} else {
					person = findPerson;
				}
			}
			
			if (countSpaces == INDENT_DIALOGUE) {
				
				String[] words = line.substring(INDENT_DIALOGUE).split(" ");
				
				for(String word : words) {
					Words wordDialog = new Words();
					wordDialog.setSetting(setting.getId());
					wordDialog.setNameSetting(setting.getName());
					wordDialog.setPerson(person.getId());
					wordDialog.setNamePerson(person.getName());
					wordDialog.setWord(word);
					wordsDAO.save(wordDialog);					
				}
			}
		}
		log.info("====== end read movie script =========");
		
		input.close();
	}
	
	private static final String HYPHEN_NAME_SETTING = "-";
	private static final String EXT_NAME_SETTING = "EXT.";
	private static final String INT_NAME_SETTING = "INT.";
	
	private String tryConvertNameSetting(String nameSetting) {
		
		log.debug("Name Setting received: {}", nameSetting);
		
		int indexHyphen = nameSetting.indexOf(HYPHEN_NAME_SETTING);
		int indexEXT    = nameSetting.indexOf(EXT_NAME_SETTING);
		int indexINT    = nameSetting.indexOf(INT_NAME_SETTING);
		
		String nameSettingConverted = new String(nameSetting);
		
		if(indexHyphen!=-1) {
			nameSettingConverted = nameSettingConverted.substring(0, indexHyphen);
		}
				
		if(indexINT > indexEXT) {
			nameSettingConverted = nameSettingConverted.substring(indexINT + INT_NAME_SETTING.length());
		} else {
			if(indexEXT != -1) {
				nameSettingConverted = nameSettingConverted.substring(indexEXT + EXT_NAME_SETTING.length());
			}
		}
		
		nameSettingConverted = nameSettingConverted.trim();

		log.debug("Name Setting converted: {}", nameSettingConverted);
		return nameSettingConverted;
	}

	private boolean isMovieSettings(String line) {
		boolean result = false;
		
		if((line.startsWith("INT.") || line.startsWith("EXT.") ) 
				&& line.equals(line.toUpperCase())) {
			result = true;
		}
		
		return result;
	}

	private int startWithHowMuchSpaces(String line) {
		int countSpaces = 0; 
		
		for(char letter : line.toCharArray()) {
			if(letter != ' ') {
				break;
			}
			countSpaces++;
		}
		
		return countSpaces;
	}
}

