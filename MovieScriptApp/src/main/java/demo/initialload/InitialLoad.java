package demo.initialload;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
class InitialLoadCommandLineRunner implements CommandLineRunner {

	private final Logger log = LoggerFactory.getLogger(InitialLoadCommandLineRunner.class);
	
	@Autowired
	private MovieScriptLoad movieScript;
	
	@Override
	public void run(String... arg0) throws Exception {

		log.info("start initial movie script load...");
		
		String scriptMovieFile = "bestMovieEver.txt";
		
		ClassLoader classLoader = getClass().getClassLoader();
		File bestMovieEver = new File(classLoader.getResource(scriptMovieFile).getFile());
		
		BufferedReader movieScriptFile = new BufferedReader(new FileReader(bestMovieEver));
		
		//movieScript.process(movieScriptFile);
		
		log.info("end initial movie script load...");
	}
}
