package demo.exceptions;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import demo.rest.services.SettingServices;
import javassist.NotFoundException;

@ControllerAdvice
public class DefaultException {

	private final Logger log = LoggerFactory.getLogger(SettingServices.class);
	
	@ExceptionHandler
	void handleIOException(NotFoundException e, HttpServletResponse response) throws Exception {
		log.error(e.getMessage());
		response.sendError(HttpStatus.NOT_FOUND.value(), e.getMessage());
	}

	@ExceptionHandler
	void handleIOException(Exception e, HttpServletResponse response) throws IOException {
		log.error(e.getMessage());
		response.sendError(HttpStatus.NOT_FOUND.value(), "Unexpected error");
	}

}


