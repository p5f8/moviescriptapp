package demo.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Words  {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	private Long setting;
	private String nameSetting;
	private Long person;
	private String namePerson;
	private String word;
	private int total;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getSetting() {
		return setting;
	}
	public void setSetting(Long setting) {
		this.setting = setting;
	}
	public String getNameSetting() {
		return nameSetting;
	}
	public void setNameSetting(String nameSetting) {
		this.nameSetting = nameSetting;
	}
	public Long getPerson() {
		return person;
	}
	public void setPerson(Long person) {
		this.person = person;
	}
	public String getNamePerson() {
		return namePerson;
	}
	public void setNamePerson(String namePerson) {
		this.namePerson = namePerson;
	}
	public String getWord() {
		return word;
	}
	public void setWord(String word) {
		this.word = word;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
}
