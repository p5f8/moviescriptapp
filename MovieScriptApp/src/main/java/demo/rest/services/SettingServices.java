package demo.rest.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import demo.dao.PersonDAO;
import demo.dao.SettingDAO;
import demo.model.Setting;
import demo.rest.result.AllCharactersWords;
import demo.rest.result.AllMovieSetting;
import demo.rest.result.Person;
import demo.rest.result.SettingAndCharacter;
import demo.rest.result.WordCount;
import javassist.NotFoundException;

@Service
public class SettingServices {
	
	private final Logger log = LoggerFactory.getLogger(SettingServices.class);
	
	private static final int RETURN_10_MOST_TALKED_WORDS_BY_CHARAC = 10;
	
	@Autowired
	private EntityManager em; 
	
	@Autowired
	private SettingDAO settingDAO;
	
	public Collection<AllMovieSetting> findAllSettings() throws NotFoundException {
		
		log.info("BEGIN findAllSettings ...");
		
		List<Setting> allSetting = settingDAO.findAll();
		Collection<AllMovieSetting> allMovieSetting = getSetting(allSetting); 
		
		log.info("END findAllSettings ...");
		
		return allMovieSetting;
	}
	
	private List<AllMovieSetting> getSetting(List<Setting> allSetting) throws NotFoundException {
		
		List<SettingAndCharacter> allSettingAndCharacter = getAllCharactersBySetting();
		
		List<AllMovieSetting> allMovieSetting = new ArrayList<AllMovieSetting>();
		
		for(Setting setting : allSetting) {
			
			AllMovieSetting movieSetting = new AllMovieSetting();
			movieSetting.setId(setting.getId());
			movieSetting.setName(setting.getName());
			
			List<Person> allCharacter = new ArrayList<Person>();
			for(SettingAndCharacter settingAndCharacter : allSettingAndCharacter) {
				
				if(settingAndCharacter.getSettingId()==setting.getId()) {
					
					Person character = findCharacter(settingAndCharacter.getPersonId());
					
					allCharacter.add(character);
				}
			}
			
			movieSetting.setCharacters(allCharacter);
			allMovieSetting.add(movieSetting);
		}
		
		return allMovieSetting;
	}
	
	private List<AllCharactersWords> getAllCharactersTopWords(int mostTalkedWords) {

		List<AllCharactersWords> allCharactersWords = getAllCharactersWordsWithSum();
		
		List<AllCharactersWords> result = new ArrayList<AllCharactersWords>();
		
		int count = 0;
		
		Long oldPerson = -1L;
		
		for(AllCharactersWords acw : allCharactersWords ) {
			
			if (acw.getPersonId() != oldPerson ) {
				oldPerson = acw.getPersonId();
				count = 0;
			}
			
			count = count + 1;
			
			if(count > mostTalkedWords) { 
				continue;
			}
			
			result.add(acw);
		}
		
		return result;
	}
	
	private List<AllCharactersWords> getAllCharactersWordsWithSum() {
		String queryAllCharactersWords = 
				"SELECT NEW demo.rest.result.AllCharactersWords(person, namePerson, word, count(1) as total)"
                + "  FROM Words "
                + " GROUP BY person, namePerson, word"
				+ " ORDER BY person, namePerson, total desc";

		return em.createQuery(queryAllCharactersWords).getResultList();
	}
	
	private List<SettingAndCharacter> getAllCharactersBySetting() {
		String queryAllCharactersBSettings = 
				"SELECT NEW demo.rest.result.SettingAndCharacter(setting, nameSetting, person, namePerson)"
                + "  FROM Words "
                + " GROUP BY setting, nameSetting, person, namePerson"
				+ " ORDER BY setting, nameSetting, person, namePerson";

		return em.createQuery(queryAllCharactersBSettings).getResultList();
	}


	public AllMovieSetting find(Long id) throws NotFoundException {
		log.info("BEGIN AllMovieSetting...");
		
		Setting setting = settingDAO.findOne(id);
		if(setting==null) {
			String message = String.format("Setting id %d not found", id);
			log.error(message);
			throw new NotFoundException(message);
		}

		List<Setting> settings = new ArrayList<Setting>();
		settings.add(setting);
				
		List<AllMovieSetting> allMovieSetting = getSetting(settings);
		
		AllMovieSetting result = new AllMovieSetting();
		
		if(allMovieSetting.size() > 0) {
			result = allMovieSetting.get(0);
		}
		
		log.info("END AllMovieSetting...");
		return result;
	}
	
	@Autowired
	private PersonDAO personDAO;
	

	public List<Person> findAllCharacters() {
		log.debug("BEGIN findAllCharacters()...");
		
		List<Person> result = findAllCharacters(personDAO.findAll());
		
		log.debug("END findAllCharacters()...");
		return result;
	}
	

	public List<Person> findAllCharacters(List<demo.model.Person> listSelectedCharacters) {
		log.debug("BEGIN findAllCharacters(List<demo.model.Person> listSelectedCharacters)...");
		
		List<Person> allCharacter = new ArrayList<Person>();
		
		List<AllCharactersWords> allCharactersWords =
				getAllCharactersTopWords(RETURN_10_MOST_TALKED_WORDS_BY_CHARAC);
		
		for(demo.model.Person p : listSelectedCharacters) {
	
			Person character = new Person();
			character.setId(p.getId());
			character.setName(p.getName());
			
			List<WordCount> allWords = new ArrayList<WordCount>();
			for(AllCharactersWords characterWord : allCharactersWords) {
				
				if(characterWord.getPersonId()==character.getId()) {
					
					WordCount word = new WordCount();
					word.setWord(characterWord.getWord());
					word.setCount(characterWord.getTotal());
					
					allWords.add(word);
				}
			}
			character.setWordCounts(allWords);
			
			allCharacter.add(character);
		}
		
		log.debug("END findAllCharacters(List<demo.model.Person> listSelectedCharacters)...");
		return allCharacter;
	}

	public Person findCharacter(Long id) throws NotFoundException {
		log.info("BEGIN findCharacter(Long id)...");
		
		demo.model.Person selectedPerson = personDAO.findOne(id);
		if(selectedPerson==null) {
			String mensage = String.format("Movie character with id: %d not found", id);
			log.error(mensage);
			throw new NotFoundException(mensage);
		}
		
		List<demo.model.Person> listSelectedCharacters = new ArrayList<demo.model.Person>();
		listSelectedCharacters.add(selectedPerson);
		
		List<Person> allCharactersResult = findAllCharacters(listSelectedCharacters);
		
		Person character = new Person();
		
		if(allCharactersResult.size()>0) {
			character = allCharactersResult.get(0);
		}
		
		log.info("END findCharacter(Long id)...");
		return character;
	}

}
