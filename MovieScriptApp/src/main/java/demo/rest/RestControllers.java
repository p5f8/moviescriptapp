package demo.rest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collection;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import demo.initialload.MovieScriptLoad;
import demo.model.ReturnMessage;
import demo.rest.result.AllMovieSetting;
import demo.rest.result.Person;
import demo.rest.services.SettingServices;
import javassist.NotFoundException;

@RestController
class RestControllers {

	private final Logger log = LoggerFactory.getLogger(RestControllers.class);
	
	@Autowired
	private MovieScriptLoad movieScriptLoad;
	
	@Autowired
	private SettingServices settingServices;
	
	@RequestMapping(value="/settings", method=RequestMethod.GET)
	public Collection<AllMovieSetting> settings() throws NotFoundException {
		log.info("BEGIN /settings");
		
		Collection<AllMovieSetting> result = settingServices.findAllSettings() ;
		
		log.info("END /settings");
		return result;
	}
	
	@RequestMapping(value="/setting/{id}", method=RequestMethod.GET)
	public AllMovieSetting findOneSetting(@PathVariable Long id) throws NotFoundException {
		log.info("BEGIN /setting/{}", id);
		
		AllMovieSetting movieSetting = settingServices.find(id) ;
		
		log.info("END /setting/{}", id);
		return movieSetting;  
	}
	
	@RequestMapping(value="/characters", method=RequestMethod.GET)
	public Collection<Person> characters() {
		log.info("BEGIN /characters");
		
		List<Person> selectedCharacters = settingServices.findAllCharacters();
		
		log.info("END /characters");
		return selectedCharacters;
	}

	@RequestMapping(value="/character/{id}", method=RequestMethod.GET)
	public Person findOneCharacter(@PathVariable Long id) throws NotFoundException {
		log.info("BEGIN /character/{}", id);
		
		Person character = settingServices.findCharacter(id);
		
		log.info("END /character/{}", id);
		return character;
	}
	
	@RequestMapping(value = "/script", method = RequestMethod.POST)
	public @ResponseBody ReturnMessage script(@RequestParam("file") MultipartFile file ) {

		ReturnMessage result = new ReturnMessage();
		
		try {
			if(!file.isEmpty()) {
				BufferedReader movieScript = new BufferedReader(new InputStreamReader(file.getInputStream()));
				
				movieScriptLoad.process(movieScript);
				result.setMessage("Movie script successfully received");
			} else {
				result.setMessage("Error data not received.");
			}
		} catch (IOException e) {
			result.setMessage("Error reading inputstream POST /script: " + e.getMessage());
			log.error(result.getMessage());
		} catch (Exception e) {
			result.setMessage("Error reading inputstream POST /script: " + e.getMessage());
			log.error(result.getMessage());
		}
		
		return result;
	}
	
}
