package demo.rest.result;

import java.util.List;

public class Person {
	
	private Long id;
	private String name;
	private List<WordCount> wordCounts;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<WordCount> getWordCounts() {
		return wordCounts;
	}
	public void setWordCounts(List<WordCount> wordCounts) {
		this.wordCounts = wordCounts;
	}
}
