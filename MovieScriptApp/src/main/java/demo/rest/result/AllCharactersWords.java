package demo.rest.result;

public class AllCharactersWords  {
	
	private Long personId;
	private String namePerson;
	private String word;
	private long total;
	
	public AllCharactersWords() { }

	public AllCharactersWords(Long personId, String namePerson, String word, long total) {
		super();
		this.personId = personId;
		this.namePerson = namePerson;
		this.word = word;
		this.total = total;
	}

	public Long getPersonId() {
		return personId;
	}

	public String getNamePerson() {
		return namePerson;
	}

	public String getWord() {
		return word;
	}

	public long getTotal() {
		return total;
	}
}
