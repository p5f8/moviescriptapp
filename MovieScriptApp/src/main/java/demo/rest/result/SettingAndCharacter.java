package demo.rest.result;

public class SettingAndCharacter {

	private Long settingId;
	private String nameSetting;
	private Long personId;
	private String namePerson;
	
	public SettingAndCharacter() { }
	
	public SettingAndCharacter(Long settingId, String nameSetting, Long personId, String namePerson) {
		this.settingId = settingId;
		this.nameSetting = nameSetting;
		this.personId = personId;
		this.namePerson = namePerson;
	}

	public Long getSettingId() {
		return settingId;
	}

	public String getNameSetting() {
		return nameSetting;
	}

	public Long getPersonId() {
		return personId;
	}

	public String getNamePerson() {
		return namePerson;
	}
	
}
