package movie;

import javax.transaction.Transactional;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import demo.MovieScriptApp;
import demo.rest.services.SettingServices;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = MovieScriptApp.class)
public abstract class MoviceScriptAppTests {
	
	private final Logger log = LoggerFactory.getLogger(this.getClass());
}

@Transactional
class AllSettingsServiceTest extends MoviceScriptAppTests {
	
	@Autowired
	private SettingServices service;
	
	@Before
	public void setUp() {
	}
	
	@org.junit.After
	public void tearDown() {
	}
	
	
}


