package demo.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Words.class)
public abstract class Words_ {

	public static volatile SingularAttribute<Words, Integer> total;
	public static volatile SingularAttribute<Words, Long> person;
	public static volatile SingularAttribute<Words, String> namePerson;
	public static volatile SingularAttribute<Words, Long> id;
	public static volatile SingularAttribute<Words, String> word;
	public static volatile SingularAttribute<Words, Long> setting;
	public static volatile SingularAttribute<Words, String> nameSetting;

}

